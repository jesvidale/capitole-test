export const state = () => ({
  list: [],
  size: 0
})

export const getters = {
  getCategoriesFromStore: state => state.list,
  getCategoryById: state => id => {
    const category = state.list.find(el => el.id === id)
    let result = ''
    if (category !== undefined && category !== null) {
      if(Object.prototype.hasOwnProperty.call(category, 'name')) {
        result = category.name
      }
    }
    return result
  },
  hasElements: state => !!state.size
}

export const actions = {
  refreshCategories({ commit }, categories) {
    if (categories.length) {
      commit('REFRESH_CATEGORIES', categories)
    }
  }
}

export const mutations = {
  REFRESH_CATEGORIES(state, categories) {
    state.list = [ ...categories ]
    state.size = categories.length
  }
}
