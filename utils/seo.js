export const toSlug = elem => {
  let parsedResp = ''
  if (elem !== null && elem !== undefined) {
    if (elem.match(/[a-z]/i)) {
      parsedResp = elem.toLowerCase()
        .replace(/ \/ /g,'-')
        .replace(/´/g,'-́')
        .trim()
        .replace(/ +(?= )|,|\./g,'')
        .replace(/ /g,'-')
    }
  }
  return parsedResp
}
