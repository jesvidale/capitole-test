import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Index from '@/pages/index'
import { getters as categoryGetters } from '@/store/categories'

const { Store } = Vuex
const localVue = createLocalVue()

localVue.use(Vuex)

describe('index.vue', () => {
  let modules
  let getters
  let actions
  let store
  let wrapper

  beforeEach(() => {
    modules = {
      categories: {
        state: {},
        categoryGetters
      }
    }
    getters = {
      'categories/hasElements': jest.fn()    }
    actions = {
      'categories/refreshCategories': jest.fn()
    }
    store = new Store({
      modules,
      getters,
      actions
    })
    wrapper = shallowMount(Index, {
      stubs: ['nuxt-link'],
      store,
      localVue,
    })
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('displays navigation component', () => {
    expect(wrapper.find('navigation-stub'))
  })
})
