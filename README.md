# The movie site

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate

# generate tests
$ npm run tests
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).


## Tasks to be done in this project

**FE VUE PROJECT**

This site shows an online store example application with the folling requirements:

- Build tool: Webpack (Nuxt)
- API: [TheMovieDatabase API](https://developers.themoviedb.org/3/getting-started/introduction)
- The app contains:
  - A home page listing several categories of items
  - A category page (with pagination) to show its items
  - A detail page of an item with detailed information
- Features:
  - An "Add to cart" button in item's detail page
  - A cart section to manage the products already added
  - Live search
  - Server Side Rendering
- A minimum of clean CSS
- Some tests with JEST
- A README file with some documentation

### Considerations

1. The payment gateway is not implemented as it is not the object of this test.
2. Prices are not shown whithin the products because the API does not bring that option.
3. Sometimes the application cannot serve a single icon. These are served through a CDN which sometimes can fail due to speed connection.
4. Pagination among products omitted.
5. Only a few components contain testing, but not all.
